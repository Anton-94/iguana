<?php

namespace App\Http\Controllers;


use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('updated_at', 'desc')->paginate(10);
        $count = 1;
        return view('home',[
            'posts' => $posts,
            'count' => $count
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function singlePost($id)
    {
        $post = Post::where('id', '=', $id)->first();
        $comments = Comment::where('post_id', $id)->orderBy('created_at','desc')->get();

        return view('singlePost',[
            'post' => $post,
            'comments' => $comments
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function comment(Request $request)
    {
        $text = $request->input('text');
        $user_id = Auth::id();
        $post_id = $request->input('post_id');

        $comment = Comment::create([
            'text' => $text,
            'user_id' => $user_id,
            'post_id' => $post_id
        ]);

        return $comment;
    }

    /**
     * TODO
     * @param $id
     */
    public function deleteComment($id)
    {
        Comment::destroy($id);
    }

}
