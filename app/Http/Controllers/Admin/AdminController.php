<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        $ip_address = $request->server('REMOTE_ADDR');
        return view('admin.main',[
            'title' => 'Admin panel',
            'ip_address' => $ip_address
        ]);
    }

}