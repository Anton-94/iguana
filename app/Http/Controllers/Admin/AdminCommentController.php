<?php

namespace App\Http\Controllers\Admin;

use App\Models\Comment;
use Illuminate\Support\Facades\Auth;

class AdminCommentController
{
    public function index()
    {
        $comments = Comment::where('user_id', Auth::user()->id)->paginate(10);

        return view('admin.comments',[
            'title' => 'Comments',
            'comments' => $comments
        ]);
    }

}