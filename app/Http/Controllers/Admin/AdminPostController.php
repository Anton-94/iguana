<?php
namespace App\Http\Controllers\Admin;

use App\Classes\Uploader;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUpdatePost;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;


class AdminPostController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('id','desc')->paginate(10);

        return view('admin.posts',[
            'title' => 'All posts',
            'posts' => $posts
        ]);
    }

    public function add()
    {
        return view('admin.addPost',[
            'title' => 'Add posts'
        ]);
    }

    public function addPost(CreateUpdatePost $request, Uploader $uploader)
    {
        $post = Post::create([
            'name' => $request->input('name'),
            'preview' => $request->input('preview'),
            'text' => $request->input('text'),
            'user_id' => Auth::id()
        ]);

        //добаление изображения в бд
        if($request->file('image')){
            $post->image = $uploader->processUpload($request, $request->files->keys()[0], $post);
        }

        $post->save();

        return redirect(route('admin.post.index'));
    }

    public function edit($id)
    {

        $post = Post::where('id', $id)->first();

        return view('admin.editPost',[
            'title' => 'Edit posts',
            'post' => $post
        ]);
    }

    public function editPost(CreateUpdatePost $request, $id, Uploader $uploader)
    {
        $post = Post::where('id', $id)->first();
        $post->name = $request->name;
        $post->preview = $request->preview;

        if($request->file('image')){
            $post->image = $uploader->processUpload($request, $request->files->keys()[0], $post);
        }
        $post->text = $request->text;
        $post->save();

        return redirect(route('admin.post.index'));
    }

    /**
     * @param integer $id
     * @return redirect
     */
    public function delete($id)
    {
        Post::destroy($id);
        return redirect(route('admin.post.index'));
    }
}