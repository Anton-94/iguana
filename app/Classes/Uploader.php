<?php namespace App\Classes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class Uploader
{
    protected $file,
        $request,
        $props = [],
        $uploadPath,
        $validationErrors = [];

    public $rules = [];

    /**
     * Валидация изображений
     * @param Request $request
     * @param $file
     * @param array $rules
     * @return bool
     */
    public function validate(Request $request, $file, array $rules = [])
    {

        //очистить все значения
        $this->clearState();
        $validationFailed = false;
        $this->request = $request;

        //если файл существует и файл валидный
        if ($request->hasFile($file) && $request->file($file)->isValid()) {
            //переданный файл перенести в свойство file
            $this->file = $request->file($file);
            $this->fillProps();

            //если rules массив и содержит больше чем 0 правил
            if (is_array($rules) && count($rules) > 0) {

                if(isset($rules['minSize'])) {
                    if ($this->props['size'] < $rules['minSize']) {
                        $validationFailed = true;
                        $this->validationErrors['minSize'] = 'Минимальный размер загружаемого файла - ' . $rules['minSize'];
                    }
                }

                if(isset($rules['maxSize'])) {
                    if ($this->props['size'] > $rules['maxSize']) {
                        $validationFailed = true;
                        $this->validationErrors['maxSize'] = 'Максимальный размер загружаемого файла - ' . $rules['maxSize'];
                    }
                }

                if(isset($rules['allowedExt']) && is_array($rules['allowedExt']) && count($rules['allowedExt']) > 0) {
                    if (!in_array($this->props['ext'], $rules['allowedExt'])) {
                        $validationFailed = true;
                        $this->validationErrors['allowedExt'] = 'Разрешены только следующие расширения: ' . implode(', ', $rules['allowedExt']);
                    }
                }

                if(isset($rules['allowedMime']) && is_array($rules['allowedMime']) && count($rules['allowedMime']) > 0) {
                    if (!in_array($this->props['mime'], $rules['allowedMime'])) {
                        $validationFailed = true;
                        $this->validationErrors['allowedMime'] = 'Разрешены только следующие MIME типы: ' . implode(', ', $rules['allowedMime']);
                    }
                }
            }
        } else {
            $validationFailed = true;
            $this->validationErrors['invalidUpload'] = 'Загрузка файла не удалась или файл поврежден';
        }

        return !$validationFailed;
    }

    public function upload($section = null)
    {
        // если section указана и существует делаем полный путь к ней, иначе формируем дефолтную секцию files
       $basePath = !is_null($section) ?  storage_path('app/public/') . $section : config('blog.uploadPath', storage_path()) . '/' . config('blog.defaultUploadSection', 'files');

        // Создание имени файла в виде хеша из 40 символов
        $newName = sha1($this->props['oldname'] . microtime(true)) . '.' . $this->getProps()['ext'];
        // Создание папок для хранения файлов, в виде: ( первый сивмол / первые три символа )
        $newDir = substr($newName, 0, 1) . '/' . substr($newName, 0, 3);

        $this->uploadPath = $newDir . '/' . $newName;

        // Создание нового пути от корня проекта к файлу
        $newPath = $basePath . '/' . $newDir;

        //если указанный путь не существует
        if (!File::exists($newPath)) {
            //создаем этот путь с правами доступа
            if (!File::makeDirectory($newPath, config('blog.storagePermissions', 0755), true)) {
                //если не удалось создать выкидываем исключение
                throw new \ErrorException('Не могу создать директорию ' . $newPath);
            }
        }
        //проверяем если путь является директорией и она доступна для записи
        if (File::isDirectory($newPath) && File::isWritable($newPath)) {
            //перемещаем файл newName в указанный путь newPath
            $this->file->move($newPath, $newName);
        } else {
            //иначе выбрасываем исключение
            throw new \ErrorException('Директория ' . $newPath . ' недоступна для записи');
        }
        // проверяем существует ли файл и возвращаем путь к нему
        return File::exists($newPath . '/' . $newName) ? $this->uploadPath : false;
    }

    public function getUploadPath()
    {
        return $this->uploadPath;
    }

    /**
     * Регистрирует файл в базе данных
     * @param Upload $uploadModel
     * @return mixed
     */
    public function register($fieldTable, $uploadModel)
    {
        $uploadModel->$fieldTable = $this->uploadPath;
        $uploadModel->save();
    }

    /**
     * Выволдит ошибки если они есть
     * @return array
     */
    public function getErrors()
    {
        return $this->validationErrors;
    }

    /**
     * Выводит параметры заданные в props
     * @return mixed
     */
    public function getProps()
    {
        return $this->props;
    }

    /**
     * Очищает все свойства файла
     */
    protected function clearState()
    {
        unset($this->file, $this->request, $this->props);
        $this->validationErrors = [];
    }

    /**
     * Заполняет свойство props параметрами из оригинального файла
     */
    protected function fillProps()
    {
        $this->props['size'] = $this->file->getSize();
        $this->props['oldname'] = $this->file->getClientOriginalName();
        $this->props['ext'] = mb_strtolower($this->file->getClientOriginalExtension());
        $this->props['mime'] = $this->file->getMimeType();
    }

    public function processUpload(Request $request, $file, $model)
    {
        $this->rules = [
            'maxSize' => 10 * 1024 * 1024,
            'minSize' => 1 * 1024,
            'allowedExt' => [
                'jpeg',
                'jpg',
                'png',
                'gif',
                'bmp',
                'tiff',
                'pdf'
            ],
            'allowedMime' => [
                'image/jpeg',
                'image/png',
                'image/gif',
                'image/bmp',
                'image/tiff',
                'application/pdf'
            ],
        ];
        if ($this->validate($request, $file, $this->rules)) {
            $uploadedPath = $this->upload('images');

            if ($uploadedPath !== false) {
                 $this->register($file ,$model);
            }

            return $uploadedPath;
        }
        else {
            dump($this->getProps());
            dump($this->getErrors());
        }

        return $this->getErrors();
    }
}