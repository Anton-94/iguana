<?php

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/single/{id}', 'HomeController@singlePost')->name('singlePost');

Route::post('/comment/add', 'HomeController@comment')->name('comment.add');

Route::post('/comment/delete/{id}', 'HomeController@deleteComment')->name('comment.delete');

