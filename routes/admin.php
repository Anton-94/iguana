<?php

Route::group(['prefix' => '/admin', 'middleware' => ['auth']], function () {

    Route::get('/', 'AdminController@index')
        ->name('admin.index');

    Route::get('/comments', 'AdminCommentController@index')
        ->name('admin.comments');


        Route::group(['prefix' => '/post'], function () {
            Route::get('/', 'AdminPostController@index')
                ->name('admin.post.index');

            Route::get('/add', 'AdminPostController@add')
                ->name('admin.post.add');

            Route::post('/add', 'AdminPostController@addPost')
                ->name('admin.post.addPost');

            Route::get('/edit/{id}', 'AdminPostController@edit')
                ->where('id', '[0-9]+')
                ->name('admin.post.edit');

            Route::post('/edit/{id}', 'AdminPostController@editPost')
                ->where('id', '[0-9]+')
                ->name('admin.post.editPost');

            Route::get('/delete/{id}', 'AdminPostController@delete')
                ->where('id', '[0-9]+')
                ->name('admin.post.delete');

            Route::post('/preview', 'AdminPostController@preview');

        });

});