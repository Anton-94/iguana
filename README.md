# Installing The Application

**Quick start**

*Clone repository*

``` git clone https://Anton-94@bitbucket.org/Anton-94/iguana.git ``` 

**Config .env**

To create a database and change the config .env

**Migrations**

Then run these command to migrate tables

``` php artisan migrate ```

**Database seeders**

 ``` php artisan db:seed ```
 
 
**Storage link**

You must create a storage link to display images

 ``` php artisan storage:link ```
 
**Mail**

I used the Mailtrap service to verify the password reset

You can also use this service https://mailtrap.io

Done! Good luck!
 