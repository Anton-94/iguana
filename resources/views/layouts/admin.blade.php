
<!DOCTYPE html>
<html lang="ru">
<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Link -->
        
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,500,900&amp;subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="{{ url('css/admin/libs.min.css')}}">
        <link rel="stylesheet" href="{{ url('css/admin/main.css')}}">
        <link rel="stylesheet" href="{{ url('css/admin/animate.css')}}">
        <link rel="stylesheet" href="{{ url('css/admin/uploadImage.css')}}">
        <link rel="stylesheet" href="{{ url('css/admin/comments.css')}}">
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
        <link rel="icon" href="{{url('/images/favicon.ico')}}" type="image/x-icon" />
        <link rel="shortcut icon" href="{{url('/images/favicon.ico')}}" type="image/x-icon" />

    <title>{{$title}}</title>
    </head>
    <body>

        <div id="wrapper">
            @include('admin.parts.header')

            @include('admin.parts.sidebar')
            <div id="page-content-wrapper" class="page-content-wrapper">
                @yield('content')
            </div>
        </div>

        <!-- Optional JavaScript -->
        <script src="https://use.fontawesome.com/5bdd64fe60.js" asunc></script>
        <script src="{{url('js/admin/libs.min.js')}}"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
        {{--<script src="{{url('js/admin/wow.min.js')}}"></script>--}}
        <script src="{{url('js/admin/common.js')}}"></script>
        <script src="{{url('js/admin/uploadImage.js')}}"></script>

    </body>
</html>