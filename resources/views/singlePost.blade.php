@extends('layouts.app')

@section('content')
    <div id="page">
        <div id="gtco-main">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <article class="mt-negative">
                            <div class="text-left content-article">
                                <img id="single-img" src="{{Storage::disk('local')->url('images/' . $post->image) }}" alt="">
                                <h1>{{ $post->name }}</h1>

                                <div class="row">
                                    {!! $post->text !!}
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="row">
                    @include('comments')
                </div>
            </div>
        </div>
    </div>
@endsection