<div class="container-fluid">
    <h2>Komentarze</h2>
    <div class="row">
        <div class="row col-6 one-comment-block">
            @foreach($comments as $comment)
                <div class="comment-wrapper">
                    <div class="col-12 mb-10" id="comment-block">
                        <div class="panel panel-white comment panel-shadow bg-white">
                            <div class="comment-heading">
                                <div class="pull-left meta">
                                    <div class="title h5">
                                        <b id="comment-user">{{$comment->user->name}}</b>
                                    </div>
                                    <h6 class="text-muted time"
                                        id="comment-date">{{$comment->created_at->diffForHumans()}}</h6>
                                </div>
                            </div>
                            <div class="comment-description">
                                <p id="comment-text">{{$comment->text}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row col-6">
            <div class="comments col-12">
                <div class="comment-wrap">
                    @if(!Auth::check())
                        <p>Komentować można tylko po zalogowaniu.</p>
                    @else
                        <form action="{{ route('comment.add') }}" id="comment-form">
                            <div class="comment-block">
                                <textarea id="text" name="text" cols="30" rows="3"
                                          placeholder="Dodaj nowy komentarz..."></textarea>
                            </div>
                            <input type="hidden" id="post-id" name="post_id" value="{{$post->id}}">
                            <button class="button pull-right" id="add-comment">Dodaj nowy komentarz</button>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@include('parts.comment-wrapper-none')

@if(Auth::check())
    <script src="{{url('js/admin/jquery-3.3.1.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#comment-form').submit(function (e) {
                e.preventDefault();
                $.ajax({
                    url: $('#comment-form').attr('action'),
                    type: "POST",
                    data: {
                        text: $("#text").val(),
                        post_id: $("#post-id").val(),
                        _token: '{{csrf_token()}}'
                    },
                    success: function (result) {
                        if (result) {
                            $(".one-comment-block").prepend($("#comment-wrapper-none").html());
                            $("#comment-user").html('{{Auth::user()->name}}');
                            $("#comment-text").html(result['text']);
                            $("#comment-date").html('{{ \Illuminate\Support\Carbon::now()->diffForHumans() }}');
                            $("#text").val('');
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            });
        });
    </script>
@endif
