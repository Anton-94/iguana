<div id="comment-wrapper-none" style="display: none;">
    <div class="comment-wrapper">
        <div class="col-12 mb-10" id="comment-block">
            <div class="panel panel-white comment panel-shadow bg-white">
                <div class="comment-heading">
                    <div class="pull-left meta">
                        <div class="title h5">
                            <b id="comment-user"></b>
                        </div>
                        <h6 class="text-muted time"
                            id="comment-date"></h6>
                    </div>
                </div>
                <div class="comment-description">
                    <p id="comment-text"></p>
                </div>
            </div>
        </div>
    </div>
</div>