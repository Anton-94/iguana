@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4 class="page-title"></h4></div>
            <div class="col-lg-12">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="white-box">
                    <div class="box-title">Komentarze</div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col" class="author-th">Autor</th>
                                <th scope="col" class="comment-th">Komentarz</th>
                                <th scope="col" class="response-th">Wpis</th>
                                <th scope="col" class="sended-th">Data</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($comments as $comment)
                                <tr>
                                    <td class="author-col" scope="row">
                                        <div class="author-box">
                                            <div class="text-box">
                                                <div class="name"><strong></strong></div>
                                                <div class="mail"><a href="#">{{$comment->user->email}}</a></div>
                                            </div>

                                        </div>
                                    <td class="comment">
                                        {{$comment->text}}
                                    </td>
                                    <td>
                                        <div class="title"><a href="#"><strong>{{$comment->post->name}}</strong></a></div>
                                        <div class="get-page"><a href="{{ route('singlePost',$comment->post->id) }}">Zobacz wpis</a></div>
                                    </td>
                                    <td>
                                        <div class="sended-text"><a href="#">{{$comment->created_at->format('d-m-Y')}}</a></div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $comments->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection