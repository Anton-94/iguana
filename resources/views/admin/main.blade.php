@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4 class="page-title">Strona główna</h4></div>
            <div class="col-lg-12">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="white-box">
                    <p>IP Adres: {{$ip_address}}  </p>
                </div>
            </div>
        </div>
    </div>
@endsection