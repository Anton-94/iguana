@extends('layouts.admin')
<div id="page-content-wrapper" class="page-content-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4 class="page-title"></h4></div>
            <div class="col-lg-12">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="white-box">
                    <div class="box-title">Dodać wpis</div>
                    <form method="post" action="{{route('admin.post.addPost')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            @if ($errors->has('name'))
                                <div class="form-group has-error col-lg-4">
                                    @else
                                        <div class="form-group  col-lg-4">
                                            @endif
                                            <label for="exampleInputEmail1">Nazwa</label>
                                            <input type="text" class="form-control" name="name"
                                                   placeholder="Nazwa wpisu..." value="{{old('name')}}"></div>
                                        @if ($errors->has('name'))
                                            <span class="error-message"
                                                  style="color: red">{{ $errors->first('name') }}</span>
                                        @endif
                                </div>
                                <div class="row">
                                    @if ($errors->has('preview'))
                                        <div class="form-group has-error col-lg-4">

                                            @else
                                                <div class="form-group  col-lg-4">
                                                    @endif
                                                    <label for="exampleInputEmail1">Opis</label>
                                                    <input type="text" class="form-control" name="preview"
                                                           placeholder="Opis..." value="{{old('preview')}}"></div>
                                                @if ($errors->has('preview'))
                                                    <div class="row">
                                                        <span class="error-message "
                                                              style="color: red">{{ $errors->first('preview') }}</span>
                                                    </div>
                                                @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Obrazek</label>
                                            <div id="imgTitle">
                                            </div>
                                            <input type="file" name="image" id="imgInp" value="{{old('image')}}">
                                        </div>

                                        <div class="form-group">
                                            <label for="text">Tekst</label>
                                            <textarea class="form-control" name="text" id="text">{{old('tekst')}}</textarea>
                                        </div>
                                        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">
                                            Dodaj
                                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{url('js/admin/jquery-3.3.1.min.js')}}"></script>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imgTitle').find('img').remove().end().prepend($('<img>', {
                    src: e.target.result,
                    style: 'height: 232px;width: 450px;'
                }))
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function () {
        readURL(this);
    });
</script>