@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4 class="page-title"></h4> </div>
            <div class="col-lg-12">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="white-box">
                    <div class="box-title">Lista wpisów </div>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr class="text-muted">
                                <th class="hidden-xs text-center">ID</th>
                                <th class="col-md-6">Nazwa</th>
                                <th class="hidden-xs col-md-2">Autor</th>
                                <th class="hidden-xs hidden-sm">Data</th>
                                <th class="col-md-1"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td class="hidden-xs text-center">{{$post->id}}</td>
                                    <td>
                                        {{$post->name}}
                                    </td>
                                    <td class="hidden-xs">
                                        {{$post->user->name}}
                                    </td>
                                    <td  class="hidden-xs hidden-sm col-md-2">
                                        {{$post->created_at}}
                                    </td>
                                    <td>
                                        <a id="edit-post" href="{{ route('admin.post.edit',$post->id)}}" data-id="1" data-name="Название" data-author="Admin" class="btn btn-info">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a href="{{ route('admin.post.delete',$post->id)}}" id="delete-user" class="btn btn-danger">
                                            <i class="fa fa-times"></i>
                                        </a>

                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            {{ $posts->links() }}
                        </div>
                        <div class="col-md-3 post_add">
                            <a href="{{route('admin.post.add')}}" class="btn btn-info waves-effect waves-light">Dodaj wpis</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection