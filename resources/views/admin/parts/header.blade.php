<div class="nav-top-wrapper">
    <div class="left-part show-logo">
        <i class="fa fa-cogs"></i>
        <div class="logo">  Admin <span>panel</span></div>
    </div>
    <ul class="nav navbar-top-links navbar-left">
        <li><a href="#" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </li>
    </ul>
    <ul class="nav navbar-top-links navbar-right pull-right">
        <li id="drop-user" class="dropdown">
            <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"><span id="span-user-name" class="hidden-xs">Admin</span><span class="caret"></span> </a>
            <ul class="dropdown-menu user-drop-menu dropdown-menu-right">
                <li>
                    <div class="drop-user-box">
                        <div class="user-text">
                            <span class="u-name-menu">Admin</span>
							<span class="label label-danger">Admin</span>
                            <p id ="p-user-email"class="text-muted">toxa404</p>
                        </div>
                    </div>
                </li>
                <li class="clear"></li>
                <li class="divider"></li>
                <li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                        {{ csrf_field() }}
                        <button>
                            <span class="icon off"><i class="fa fa-power-off" aria-hidden="true"></i></span>
                            Wyloguj
                        </button>
                    </form>

                </li>
            </ul>
        </li>
    </ul>
</div>