<!-- Sidebar -->
<div id="sidebar-wrapper" class="hide-md">
    <div class="sidebar-head">
        <a href="#" id="menu-toggle"><span class="fa fa-times"></span></a>
        <span class="hide-menu">Nawigacja</span>
    </div>
    <ul class="sidebar-nav" id="sidebar-nav">
        <li><a  href="{{route('admin.index')}}"><i class="fa fa-home"></i>Strona główna</a></li>
        <li><a  href="{{route('admin.post.index')}}"><i class="fa fa-file-text"></i>Wpisy</a></li>
        <li><a  href="{{route('admin.comments')}}"><i class="fa fa-file-text"></i>Komentarze</a></li>
    </ul>
</div>