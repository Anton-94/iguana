@extends('layouts.app')

@section('content')
    <div id="page">
        <div id="gtco-main">
            <div class="container">
                <div class="row row-pb-md">
                    <div class="col-md-12">
                        <ul id="gtco-post-list">
                            @foreach($posts as $post)
                                @if($count == 1)
                                    <li class="full entry animate-box" data-animate-effect="fadeIn">
                                @elseif($count == 2 or $count == 9)
                                    <li class="two-third entry animate-box" data-animate-effect="fadeIn">
                                @elseif($count == 7)
                                    <li class="one-half entry animate-box" data-animate-effect="fadeIn">
                                @elseif($count == 8)
                                    <li class="one-half entry animate-box" data-animate-effect="fadeIn">
                                @else
                                    <li class="one-third entry animate-box" data-animate-effect="fadeIn">
                                @endif
                                    <a href="{{ route('singlePost', $post->id) }}">
                                        <div class="entry-img" style="background-image: url({{Storage::disk('local')->url('images/' . $post->image) }}"></div>
                                        <div class="entry-desc">
                                            <h3>{{ $post->name }}</h3>
                                            <p>{{ $post->preview }}</p>
                                        </div>
                                    </a>
                                    <a href="{{ route('singlePost', $post->id) }}" class="post-meta"><span
                                                class="date-posted">{{ $post->created_at->diffForHumans() }}</span></a>
                                </li>
                                @php $count++ @endphp
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center pagination">
                        <nav aria-label="Page navigation">
                            {{ $posts->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
