<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'user_id' => 1,
            'name' => 'Database: Seeding',
            'preview' => 'Database: Seeding Introduction Writing Seeders Using Model Factories Calling Additional Seeders Running Seeders  Introduction Laravel includes a simple method of seeding your database with test data using seed classes. All seed classes are stored in the database/seeds directory. Seed classes may have any name you wish, but probably should follow some sensible convention, such as UsersTableSeeder, etc. By default, a DatabaseSeeder class is defined for you. From this class, you may use the call method to run other seed classes, allowing you to control the seeding order.',
            'text' => '<h2 style="-webkit-font-smoothing: antialiased; font-family: &quot;Whitney A&quot;, &quot;Whitney B&quot;, sans-serif; margin-top: 55px; margin-bottom: 15px; position: relative; color: rgb(82, 82, 82);"><p style="margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc viverra quam id sem pellentesque, vel egestas velit fringilla. Vestibulum dapibus nec dolor vel suscipit. Donec mollis quam ac fringilla suscipit. Maecenas lobortis, dolor eget pellentesque congue, nulla leo dignissim quam, a faucibus massa mi vitae nulla. Curabitur pulvinar tincidunt augue eu consequat. Nulla facilisi. Cras sit amet ipsum ut leo maximus laoreet porttitor varius sem. Sed eget sollicitudin nulla. Donec augue nisl, scelerisque quis ultricies sit amet, suscipit a nisi. Morbi gravida orci neque, quis fringilla tellus elementum ut.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;">Mauris at elit vel felis porttitor sagittis. Suspendisse et porta risus, id tempus enim. Quisque et ornare urna. Fusce ut justo urna. Integer sollicitudin suscipit eros non gravida. Aenean aliquet leo scelerisque, dapibus augue non, varius ante. Sed vitae est sodales, fringilla odio sit amet, elementum odio. Quisque dapibus lorem nec auctor pharetra. Pellentesque ut mattis est, nec blandit felis. Nunc at ullamcorper purus. Integer interdum lacus a purus dictum, et tempus erat tempor.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;">Donec pulvinar lectus nec metus aliquet, ut fermentum purus congue. Suspendisse potenti. Aenean vitae varius leo, at convallis metus. Mauris faucibus mi at venenatis pellentesque. Nam eros lectus, aliquet et lorem posuere, semper maximus tellus. Etiam sit amet eleifend leo. Nullam fringilla sed lectus eget aliquam.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;">Vestibulum posuere gravida quam eu finibus. Sed tempus pretium ex varius euismod. Praesent id porta ante, sed efficitur erat. Cras eu ante sit amet purus ullamcorper consequat. Donec malesuada vulputate vehicula. Vivamus iaculis sem eget turpis aliquam, quis imperdiet turpis sodales. Pellentesque malesuada vitae elit nec semper. Curabitur pulvinar, dui sed dapibus pulvinar, purus nisl suscipit arcu, ut dictum lectus augue vitae purus. Proin rutrum tempus risus et facilisis. Fusce ac eleifend neque. Morbi bibendum, enim vel facilisis suscipit, sapien justo tristique massa, sed blandit eros ligula eu ipsum. Pellentesque elementum bibendum pulvinar. Ut sagittis at erat semper molestie. Aliquam erat volutpat.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;">Pellentesque efficitur faucibus consequat. Suspendisse ullamcorper sagittis ante, at convallis nibh imperdiet nec. Donec orci ligula, interdum ut mattis ut, imperdiet a odio. Sed nec quam elementum, pellentesque augue et, ultrices nisi. In sit amet lorem faucibus, ultricies odio eget, mollis quam. Aenean aliquam tempor nisi, sed bibendum eros sollicitudin at. Cras fringilla lorem metus, molestie dictum metus efficitur et.</p></h2>',
            'image' => 'b/b92/b92915e106cb9caeafd28de2fb655c7f34273abd.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
